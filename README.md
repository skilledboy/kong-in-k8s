## Documentación para la instalación de KongGateway
A continuación explicaremos como hacer una instalación del gateway de Kong en un cluster kubernetes.

#### Requisitos para la instalación y configuración:
1 - Poseer skills trabajando con kubernetes, helm y línea de comandos. \
2 - Tener un cluster kubernetes configurado y operativo. \
3 - kubernetes-cli y Helm deben de estar instalados y con el contexto del cluster configurado. \
4 - Se debe tener dos bases datos accesibles desde el cluster para guardar las configuraciones del gateway y de la interfaz de administración(konga) o en su defecto una base de datos con dos schemas.

Teniendo los requisitos claros, comenzaremos con la instalación, para ello tendremos un archivo llamado "values.yaml", el cual tendrá toda la configuración necesaria para el despliegue del gateway y los services necesarios para la administración y el consumo de las apis.

**Consideraciones sobre el archivo "values.yaml" :**
 - El "values.yaml" debe ser editado y adecuado a las necesidades de cada ambiente(desarrollo, qa o producción).
 - Si se instala con helm en su versión 3.x no es necesario habilitar el parámetro "installCRDs: false".


Teniendo las herramientas y las configuraciones descritas hasta ahora listas, comenzaremos con la instalación del gateway. \
***Recordar siempre revisar los yaml antes de aplicarlos, y de ser necesario actualizar sus datos(namespace, value de los secrets, etc...)*** \
### Pasos:
1 - Desplegar los secretos que contendrán las credenciales de la base de datos.
```
kubectl apply -f kong-secret.yaml
```
2 - Agregar el repo de charts para helm.
```
helm repo add kong https://charts.konghq.com
helm repo update
```
Al agregar este repositorio estamos diciéndole a helm de donde instalará KongGateway.

3 - Instalación de kong:
Antes de ejecutar el comando para que se configure kong en el cluster debemos revisar los parámetros del archivo "values.yaml" y ajustar sus valores a lo que necesitamos y al ambiente en que lo ejecutaremos (desarrollo, qa, producción).
Ejemplo de los parámetros que debemos ajustar:
 - Datos de conexión de la base de datos.
 - Versiones del ingress controller.
 - Secretos.
 - Puertos.
Luego de ver revisado nuestro value, ya podemos ejecutar su instalación:
```
helm install kong-deploy kong/kong --version 0.36.2 --namespace kong -f values.yaml
```
En el comando especificamos el nombre(kong-deploy), el chart(kong/kong), la versión que deseamos instalar (0.36.2), en qué espacio de nombre se instalará (kong) y la configuración general definida en el "values.yaml". \
-> Debemos de guardar la salida de este comando ya que usaremos algunos datos de el para la configuración de la UI de administración(konga).

4 - Desplegar la UI de administración para kong(konga):
Lo primero que debemos hacer para desplegar konga es crear el secreto con las credenciales de la base de datos que usará konga.
```
kubectl apply -f konga-secret.yaml
```
luego debemos hacer aplicar el manifests para desplegar konga, tener en cuenta que se deben actualizar el namespace y los secretos en caso de usar otros datos.
```
kubectl apply -f admingateway-deployment.yaml
```
una vez esté ejecutando correctamente, desplegamos el service asociado a este deployment. De igual forma que los demás yaml debemos revisar su configuración por si deseamos cambiar algún parámetro.
```
kubectl apply -f admingateway-svc.yaml
```

5 - Una vez que los pods de kong y konga se estén ejecutando correctamente crearemos un ingress para exponer el servicio para el acceso a las apis y a la UI de administración. \
-> Debemos revisar primero el "ingress-kong.yaml" y colocar los valores deseados como:
- namespace
- host
- es recomendable configurar un certificado ssl
```
kubectl apply -f ingress-kong.yaml
```

6 - Solo nos queda entrar al host definido para la UI de administración y agregar nostro kong para administrarlo:

![Captura de Ejemplo](img/admin-new-connection.png "Título alternativo")

Tener en cuenta que debemos cambiar "Kong Admin URL" por el valor de dns que nos dio la salida de la instalación con helm en el paso 3.

Link de documentación: \
[GitHub Repositorio Oficial](https://github.com/Kong/charts/tree/master/charts/kong) \
[Instalación Oficial Kong](https://docs.konghq.com/1.4.x/kong-for-kubernetes/install)
